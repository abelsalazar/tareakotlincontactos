package com.academiamoviles.sesion04app.model

data class Contacto(
    val nombre:String,
    val cargo:String,
    val correo:String,
    val numero:String)