package com.academiamoviles.sesion04app.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.academiamoviles.sesion04app.R
import com.academiamoviles.sesion04app.model.Contacto
import kotlinx.android.synthetic.main.item.view.*

class ContactoAdapter (
                     private var contactos: MutableList<Contacto> = mutableListOf(),
                     private val itemCallbackContacto: (item:Contacto)-> Unit) :
    RecyclerView.Adapter<ContactoAdapter.ContactoAdapterViewHolder>(){

    class ContactoAdapterViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

        fun bind(contacto: Contacto, itemCallbackContacto: (item: Contacto) -> Unit){

            itemView.tvNombres.text = contacto.nombre
            itemView.tvCargo.text = contacto.cargo
            itemView.tvCorreo.text = contacto.correo
            itemView.tvLetra.text = contacto.nombre.substring(0,1)
            itemView.imgLlamar.setOnClickListener { itemCallbackContacto(contacto) }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactoAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item,parent,false)
        return ContactoAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return contactos.size
    }

    fun actualizarLista(contactos: MutableList<Contacto>){
        this.contactos = contactos
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ContactoAdapterViewHolder, position: Int) {

        val contacto = contactos[position]

        holder.bind(contacto,itemCallbackContacto)

        //holder.itemView.setOnClickListener {
        //    itemCallbackContacto(contacto)
        //}

    }

}