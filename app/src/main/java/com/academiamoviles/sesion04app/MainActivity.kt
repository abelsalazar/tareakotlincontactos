package com.academiamoviles.sesion04app

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.academiamoviles.sesion04app.adapter.ContactoAdapter
import com.academiamoviles.sesion04app.model.Contacto
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    var contactos = mutableListOf<Contacto>()
    lateinit var adapter:ContactoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progress.visibility = View.VISIBLE

        configurarAdapter()

        mostrarDialogo()
    }

    private fun mostrarDialogo() {

        adapter = ContactoAdapter(){
            val i = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + it.numero))
            try {
                startActivity(i)
            } catch (e: Exception) {
                // this can happen if the device can't make phone calls
                // for example, a tablet
            }
        }

        recyclerContact.adapter = adapter
        recyclerContact.layoutManager = LinearLayoutManager(this)

    }


    private fun configurarAdapter() {

        adapter = ContactoAdapter(){
            Log.i("jledesma", "${it.nombre}")
        }

        recyclerContact.adapter = adapter
        recyclerContact.layoutManager = LinearLayoutManager(this)

        Handler().postDelayed({
            adapter.actualizarLista(loadData())
            progress.visibility = View.GONE
        }, 5000)

    }

    private fun loadData() : MutableList<Contacto>{

        contactos.add(
            Contacto(
                "Abel Angel Salazar",
                "Ingeniero",
                "abel.angelsc@gmail.com",
                "961104340"
            )
        )
        contactos.add(
            Contacto(
                "Luis Angel Salazar",
                "Supervisor",
                "luis.angels@gmail.com",
                "961104340"
            )
        )
        contactos.add(
            Contacto(
                "Abel Ernesto Salazar",
                "Técnico",
                "abel.ernestos@gmail.com",
                "951088035"
            )
        )

        return contactos
    }
}